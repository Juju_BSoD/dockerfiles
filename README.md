# dockerfiles

### postgis

- see [readme](postgis/readme.md)

### nominatim

- see [readme](nominatim/readme.md)

### tileserver

- see [readme](tileserver/readme.md)

### geoserver

- see [readme](geoserver/readme.md)
