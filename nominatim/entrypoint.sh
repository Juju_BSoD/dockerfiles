#!/bin/bash

# Defaults
NOMINATIM_DATA_PATH=${NOMINATIM_DATA_PATH:="/srv/data"}
NOMINATIM_DATA_LABEL=${NOMINATIM_DATA_LABEL:="data"}
THREAD_NUMBER=${THREAD_NUMBER:=4}

if [ -z "$(ls -A $NOMINATIM_DATA_PATH)" ]; then
   echo "You must bind a volume on docker run with -v {absolute path folder with pbf files}:/srv/data before run container"
	 exit -1
else
   echo ""
fi

if [[ -z "${DB_USER}" ]]; then
  echo "You must provide a database username environnement varible with -e at container run"
  exit -1
else
  echo ""
fi

if [[ -z "${DB_PASS}" ]]; then
  echo "You must provide a database password environnement varible with -e at container run"
  exit -1
else
  echo ""
fi

if [[ -z "${DB_NAME}" ]]; then
  echo "You must provide a database name environnement varible with -e at container run"
  exit -1
else
  echo ""
fi

if [[ -z "${DB_HOST}" ]]; then
  echo "You must provide a database host environnement varible with -e at container run"
  exit -1
else
  echo ""
fi

if [[ -z "${DB_PORT}" ]]; then
  echo "You must provide a database port environnement varible with -e at container run"
  exit -1
else
  echo ""
fi



cd $NOMINATIM_DATA_PATH
FILE=$NOMINATIM_DATA_PATH/$NOMINATIM_DATA_LABEL.osm.pbf
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    echo "genrate $FILE file"
    osmium merge *.pbf -o $NOMINATIM_DATA_LABEL.osm.pbf || true
fi

/srv/nominatim/build/utils/setup.php --osm-file $NOMINATIM_DATA_PATH/$NOMINATIM_DATA_LABEL.osm.pbf --all --threads $THREAD_NUMBER

# Tail Apache logs
tail -f /var/log/apache2/* &

# Run Apache in the foreground
/usr/sbin/apache2ctl -D FOREGROUND
