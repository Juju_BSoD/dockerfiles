# tileserver

- This image is base on **varnish:6.0** docker image use to provide cache ain front of tilesever-gl-light
- **It's mandatory to bond a volume with tiles.mbtiles file and config.json file with are required for tileserver-gl**

## Build image

```
docker build -t tileserver-ngms -f tileserver/Dockerfile tileserver
```

## Run image

```
docker run --rm -v $(pwd):/data \
 -p 2080:80 \
 -v $(pwd)/tileserver-data:/data
 --name tileserver-ngms tileserver-ngms

```
