#!/bin/bash

if [ -z "$(ls -A /data)" ]; then
   echo "You must bind a volume on docker run with -v {absolute path/}:/data before run container"
	 exit -1
else
   echo ""
fi

FILE=/data/tiles.mbtiles
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "missing tiles.mbtiles file in bind volume"
    exit -1
fi


FILE=/data/config.json
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "missing config.json file in bind volume"
    exit -1
fi

set -e

GL_PORT="2080"
xvfb-run --server-args="-screen 0 1024x768x24" tileserver-gl-light /data/tiles.mbtiles -p ${GL_PORT} -c /data/config.json &

sleep 5

VARNISH_SIZE=${VARNISH_SIZE:="5G"}

TILESERVER_PORT=${TILESERVER_PORT:=15020}

varnishd \
	    -F \
	    -f /etc/varnish/default.vcl \
	    -a http=:${TILESERVER_PORT},HTTP \
	    -p default_ttl=2419200\
	    -p feature=+http2 \
	    -s malloc,$VARNISH_SIZE