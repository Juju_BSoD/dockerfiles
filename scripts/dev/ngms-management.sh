#!/bin/bash

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
    
pushd -n -n $SCRIPTPATH 
website(){
  echo "Website for log : http://127.0.0.1:9000 - admin/admin" 
  echo "Website for mononitoring : http://localhost:8080"
  echo "Website for cartopoc : http://localhost:15080"
  echo "Website for openapi : http://localhost:15080"
}




initialisation(){
  docker login registry.gitlab.com/cracs_airbus/ngms -u cracs -p sb1HzR-X7jc_DaYsMDzQ >& /dev/null
}

killdocker(){
  docker kill postgis-ngms  >& /dev/null
  docker kill nominatim-ngms  >& /dev/null
  docker kill carto-poc-ngms  >& /dev/null
  docker kill tileserver-ngms  >& /dev/null
  docker kill proxy_ngms  >& /dev/null
  docker kill pgadmin-ngms  >& /dev/null 
}


killall(){

  docker kill $(docker ps -q) >& /dev/null
  docker rm $(docker ps -a -q) >& /dev/null

  echo ""
  echo ""
  echo ""
  echo ""
  echo "All docker conteneurs killed"
  echo ""
  docker ps
  
}


nuclearbomb(){
  docker kill $(docker ps -q)
  docker rm $(docker ps -a -q)
  docker rmi $(docker images -q)
}



help(){

  echo "0 -> kill all ngms containers"
  echo "1 -> Start dockers prod mode"
  echo "2 -> Start dockers dev mode" 
  echo "3 -> start front-end (npm)"
  echo "4 -> start back-end  (./mvnw clean compile quarkus:dev)" 
  echo "5 -> new install from git clone "
  echo "6 -> kill all containeurs"
  echo "7 -> nuclear bomb (kill all conteneurs and remove all images)"
}


help
echo -n "What do you want to do : "
read choose



case $choose in

  0)
    killdocker
    ;;

  1) 
     git submodule update --init --recursive
     git config --global submodule.recurse true
     pushd -n -n $SCRIPTPATH 
     echo ""
     echo ""
     echo ""
    docker-compose rm  -f 
    docker-compose pull
   
     echo ""
     echo ""
     echo ""
    docker-compose up
    docker-compose rm   -f
    ;;

  2) git submodule update --init --recursive
     git config --global submodule.recurse true
    pushd -n -n $SCRIPTPATH 

    pwd
     echo ""
     echo ""
     echo ""
    docker-compose rm -f  
    docker-compose pull
    
     echo ""
     echo ""
     echo ""
    docker-compose -f scripts/dev/docker-compose.dev.yml up 
    docker-compose rm  -f
    ;;

  3)
    git submodule update --init --recursive
    git config --global submodule.recurse true
    cd carto-poc/webapp
    npm install
    npm run generate:api
    npm start
   
    ;;

  4) pushd -n -n $SCRIPTPATH 
     cd carto-poc/
     git submodule update --init --recursive
     git config --global submodule.recurse true
    .//mvnw clean compile quarkus:dev
    ;;

  5)


    echo "

    COPY PASTE ME -> 

    git clone git@gitlab.com:cracs_Airbus/ngms/dockerfiles.git $HOME/dockerfiles
    cd dockerfiles
    git submodule update --init --recursive
    git config --global submodule.recurse true
    sudo git pull
    sudo docker-compose pull 
    "

   ;;
   
  6)
    killall
    ;;

  7)
    nuclearbomb
    ;;

  *)
    help
    ;;
esac



