#!/bin/bash
echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $GROUP_REGISTRY
docker info

echo "=========== pull image =========== "
docker pull $GROUP_REGISTRY/$IMAGE_NAME:latest || true

echo "=========== build image ==========="
docker build --pull \
  --tag $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG \
  --build-arg VCS_REF=$CI_PIPELINE_IID  \
  --build-arg VCS_URL=$CI_PROJECT_URL \
  --cache-from $GROUP_REGISTRY/$IMAGE_NAME:latest -f $DOCKEFILE_PATH $DOCKEFILE_CONTEXT

echo "=========== push image tagged with ci id ==========="
docker push $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "============ pull tagged image  =============="
docker pull $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "============  tagged image latest  =============="
docker tag $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG $GROUP_REGISTRY/$IMAGE_NAME:latest

echo "============ push latest image  =============="
docker push $GROUP_REGISTRY/$IMAGE_NAME:latest