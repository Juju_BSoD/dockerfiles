## run

```
docker run --rm --pull always -p 6543:5432 -e POSTGRES_DB=ngms -e POSTGRES_USER=root -e POSTGRES_PASSWORD=root --name postgis-ngms --memory="2G" --cpus=1.0 --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 --network=ngms-network registry.gitlab.com/cracs_airbus/ngms/dockerfiles/postgis
```

docker pull kartoza/geoserver
docker run -d -p 8600:8080 --name geoserver --link postgis-ngms:db --network=ngms-network -e DB_BACKEND=POSTGRES -e HOST=db -e POSTGRES_PORT=5432 -e POSTGRES_DB=ngms -e POSTGRES_USER=root -e POSTGRES_PASS=root kartoza/geoserver

créer espace de travail :
nom : ngms
url : /ngms

## ajouter un entrepot de données :

## type postgis

nom de la source de données : postgis-ngms
host : postgis-ngms
port : 5432
database : ngms
login : root
pass : root

Configurer une nouvelle vue SQL : (il est nécessaire d'avoir des données en base pour réaliser cette étape)
nom de la vue : ngms-poi
Directive SQL : select name, attribute_data_json, layer_id, ST_SETSRID(ST_POINT(posx, posy), 3857) as geom from ngms_poi

SLD :

```
<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>point-ngms</Name>
    <UserStyle>
      <Title>violet square point style</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>violet point</Title>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>ttf://Webdings#0x0070</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#3300ff</CssParameter>
                </Fill>
              </Mark>
              <Size>10</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
```

fonts : http://www.alanwood.net/demos/webdings.html
