#!/bin/bash

if [[ -z "${ADMIN_LOGIN}" ]]; then
   echo "You must provide ADMIN_LOGIN environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${ADMIN_PASS}" ]]; then
   echo "You must provide ADMIN_PASS environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${HOST}" ]]; then
   echo "You must provide HOST environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${POSTGRES_USER}" ]]; then
   echo "You must provide POSTGRES_USER environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${POSTGRES_PASS}" ]]; then
   echo "You must provide POSTGRES_PASS environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${POSTGRES_PORT}" ]]; then
   echo "You must provide POSTGRES_PORT environnement variable at docker run"
	 exit -1
else
   echo ""
fi

if [[ -z "${POSTGRES_DB}" ]]; then
   echo "You must provide POSTGRES_DB environnement variable at docker run"
	 exit -1
else
   echo ""
fi

LOGIN="$ADMIN_LOGIN:$ADMIN_PASS"

DB_HOST=${HOST}
DB_USERNAME=${POSTGRES_USER}
DB_PASSWORD=${POSTGRES_PASS}
DB_PORT=${POSTGRES_PORT}
DB_DATABASE=${POSTGRES_DB}

API_BASE_URL=${API_BASE_URL:="localhost"}
API_PORT=${API_PORT:="8080"}

API_URL="http://$API_BASE_URL:$API_PORT"

/ngms/wait-it.sh $API_URL -- echo "api is up"

curl -sS -u "$LOGIN" -X POST "$API_URL/geoserver/rest/workspaces?default=true" -H  "accept: text/html" -H  "content-type: application/xml" -d "<?xml version=\"1.0\" encoding=\"UTF-8\"?><workspace>\t<name>ngms</name></workspace>" 

if [[ "$(curl -s -o /dev/null -I -w '%{http_code}' $API_URL/geoserver/rest/workspaces/ngms/datastores)" == "200" ]] ; then
	printf "workspace ngms created...\n"
	if [[ "$(curl -s -o /dev/null -I -w '%{http_code}' $API_URL/geoserver/rest/namespaces/ngms)" == "200" ]] ; then
		printf "namespace correctly created...\n"
	else
		"problem with the namespace"
		exit 1
	fi
else
	echo "workspace not created"
	exit 1
fi

curl -sS -u "$LOGIN" -X PUT "$API_URL/geoserver/rest/namespaces/ngms" -H  "accept: application/xml" -H  "content-type: application/xml" -d "<namespace>
  <prefix>ngms</prefix>
  <uri>ngms</uri>
  <isolated>false</isolated>
</namespace>" > /dev/null


curl -sS -u "$LOGIN" -X POST $API_URL/geoserver/rest/workspaces/ngms/datastores -H  "accept: application/xml" -H  "content-type: application/xml" -d "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<dataStore> 
<name>postgis-ngms</name>  <enabled>true</enabled> <connectionParameters>
	<entry key=\"schema\">public</entry>
	<entry key=\"Evictor run periodicity\">300</entry>
	<entry key=\"Max open prepared statements\">50</entry>
	<entry key=\"encode functions\">true</entry>
	<entry key=\"Batch insert size\">1</entry>
	<entry key=\"preparedStatements\">false</entry>
	<entry key=\"database\">$DB_DATABASE</entry>
	<entry key=\"host\">$DB_HOST</entry>
	<entry key=\"Loose bbox\">true</entry>
	<entry key=\"SSL mode\">DISABLE</entry>
	<entry key=\"Estimated extends\">true</entry>
	<entry key=\"fetch size\">1000</entry>
	<entry key=\"Expose primary keys\">false</entry>
	<entry key=\"validate connections\">true</entry>
	<entry key=\"Support on the fly geometry simplification\">true</entry>
	<entry key=\"Connection timeout\">20</entry>
	<entry key=\"create database\">false</entry>
	<entry key=\"Method used to simplify geometries\">FAST</entry>
	<entry key=\"port\">$DB_PORT</entry>
	<entry key=\"passwd\">$DB_PASSWORD</entry>
	<entry key=\"min connections\">1</entry>
	<entry key=\"dbtype\">postgis</entry>
	<entry key=\"namespace\">ngms</entry>
	<entry key=\"max connections\">10</entry>
	<entry key=\"Evictor tests per run\">3</entry>
	<entry key=\"Test while idle\">true</entry>
	<entry key=\"user\">$DB_USERNAME</entry>
	<entry key=\"Max connection idle time\">300</entry>
</connectionParameters> </dataStore>" > /dev/null

if [[ "$(curl -s -o /dev/null -I -w '%{http_code}' $API_URL/geoserver/rest/workspaces/ngms/datastores/postgis-ngms)" == "200" ]] ; then
	printf "datastore postgis-ngms created...\n"
else
	echo "datastore not created"
	exit 1
fi

curl -sS -u "$LOGIN" -X POST $API_URL/geoserver/rest/workspaces/ngms/datastores/postgis-ngms/featuretypes -H  "accept: application/xml" -H  "content-type: application/xml" -d "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<featureType>
  <name>ngms-poi</name>
  <nativeName>ngms-poi</nativeName>
  <namespace>
    <name>ngms</name>
    <atom:link xmlns:atom=\"http://www.w3.org/2005/Atom\" rel=\"alternate\" href=\"$API_URL/geoserver/rest/namespaces/ngms.xml\" type=\"application/xml\"/>
  </namespace>
  <title>ngms-poi</title>
  <keywords>
    <string>features</string>
    <string>ngms-poi</string>
  </keywords>
  <srs>EPSG:4326</srs>
  <nativeBoundingBox>
    <minx>-2.0037508342789244E7</minx>
    <maxx>2.0037508342789244E7</maxx>
    <miny>-2.00489661040146E7</miny>
    <maxy>2.0048966104014594E7</maxy>
    <crs class=\"projected\">EPSG:3857</crs>
  </nativeBoundingBox>
  <latLonBoundingBox>
    <minx>-2.0037508342789244E7</minx>
    <maxx>2.0037508342789244E7</maxx>
    <miny>-2.00489661040146E7</miny>
    <maxy>2.0048966104014594E7</maxy>
    <crs>EPSG:4326</crs>
  </latLonBoundingBox>
  <projectionPolicy>FORCE_DECLARED</projectionPolicy>
  <enabled>true</enabled>
  <metadata>
    <entry key=\"JDBC_VIRTUAL_TABLE\">
      <virtualTable>
        <name>ngms-poi</name>
        <sql>select name, attribute_data_json, layer_id, ST_SETSRID(ST_POINT(posx, posy), 3857) as geom from ngms_poi
</sql>
        <escapeSql>false</escapeSql>
      </virtualTable>
    </entry>
  </metadata>
  <store class=\"dataStore\">
    <name>ngms:postgis-ngms</name>
    <atom:link xmlns:atom=\"http://www.w3.org/2005/Atom\" rel=\"alternate\" href=\"$API_URL/geoserver/rest/workspaces/ngms/datastores/postgis-ngms.xml\" type=\"application/xml\"/>
  </store>
  <serviceConfiguration>false</serviceConfiguration>
  <simpleConversionEnabled>false</simpleConversionEnabled>
  <maxFeatures>0</maxFeatures>
  <numDecimals>0</numDecimals>
  <padWithZeros>false</padWithZeros>
  <forcedDecimal>false</forcedDecimal>
  <overridingServiceSRS>false</overridingServiceSRS>
  <skipNumberMatched>false</skipNumberMatched>
  <circularArcPresent>false</circularArcPresent>
  <attributes>
    <attribute>
      <name>name</name>
      <minOccurs>0</minOccurs>
      <maxOccurs>1</maxOccurs>
      <nillable>true</nillable>
      <binding>java.lang.String</binding>
    </attribute>
    <attribute>
      <name>attribute_data_json</name>
      <minOccurs>0</minOccurs>
      <maxOccurs>1</maxOccurs>
      <nillable>true</nillable>
      <binding>java.lang.String</binding>
    </attribute>
    <attribute>
      <name>layer_id</name>
      <minOccurs>0</minOccurs>
      <maxOccurs>1</maxOccurs>
      <nillable>true</nillable>
      <binding>java.lang.Long</binding>
    </attribute>
    <attribute>
      <name>geom</name>
      <minOccurs>0</minOccurs>
      <maxOccurs>1</maxOccurs>
      <nillable>true</nillable>
      <binding>org.locationtech.jts.geom.Geometry</binding>
    </attribute>
  </attributes>
</featureType>" > /dev/null

if [[ "$(curl -s -o /dev/null -I -w '%{http_code}' $API_URL/geoserver/rest/workspaces/ngms/datastores/postgis-ngms/featuretypes/ngms-poi)" == "200" ]] ; then
	printf "layer ngms-poi created...\n"
else
	echo "database started ?"
	exit 1
fi

curl -sS -u "$LOGIN" -X POST $API_URL/geoserver/rest/workspaces/ngms/styles?name=point-ngms -H  "accept: application/json" -H  "Content-Type: application/vnd.ogc.sld+xml" -d "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<StyledLayerDescriptor version=\"1.0.0\"  xsi:schemaLocation=\"http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd\"
    xmlns=\"http://www.opengis.net/sld\"
    xmlns:ogc=\"http://www.opengis.net/ogc\"
    xmlns:xlink=\"http://www.w3.org/1999/xlink\"
    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
    <NamedLayer>
        <Name>point-ngms</Name>
        <UserStyle>
            <Title>violet square point style</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Title>violet point</Title>
                    <PointSymbolizer>
                        <Graphic>
                            <Mark>
                                <WellKnownName>ttf://Webdings#0x003D</WellKnownName>
                                <Fill>
                                    <CssParameter name=\"fill\">#3300ff</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>10</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>" > /dev/null

if [[ "$(curl -sS -u $LOGIN -X GET $API_URL/geoserver/rest/workspaces/ngms/styles/point-ngms -H  'accept: application/xml' -H  'content-type: application/json')" != "No such style point-ngm in workspace ngms" ]] ; then
	printf "style point-ngms created...\n"
else
	echo "style not created"
	exit 1
fi

curl -sS -u "$LOGIN" -X PUT $API_URL/geoserver/rest/layers/ngms%3Angms-poi -H  "accept: application/json" -H  "content-type: application/xml" -d "<?xml version=\"1.0\" encoding=\"UTF-8\"?><layer>  <defaultStyle>    <name>ngms:point-ngms</name>  </defaultStyle></layer>" > /dev/null

printf "style point-ngms assigned to ngms-poi...\n"
